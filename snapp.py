import time
import unittest
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction

class snapp(unittest.TestCase):
    def setUp(self):

        desired_cap ={
        "deviceName": "Galaxy A30",
        "udid": "RZ8M83M66GV",
        "platformName": "Android",
        "platformVersion": "10",
        "appPackage": "cab.snapp.passenger",
        "appActivity": "cab.snapp.passenger.activities.launcher.LauncherActivity",
        "newCommandTimeout": 600
        }

        self.driver = webdriver.Remote("http://localhost:4723/wd/hub",desired_cap)
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.quit()

    def test_take_a_ride(self):

        self.driver.find_element_by_id("cab.snapp.passenger:id/view_tour_cancel_image_view").click()

        self.driver.find_element_by_id("com.android.permissioncontroller:id/permission_allow_foreground_only_button").click()

        self.driver.find_element_by_id("cab.snapp.passenger:id/item_snapp_group_header_first_item_textview").click()

        self.driver.find_element_by_id("cab.snapp.passenger:id/view_main_location_selector_ib").click()

        self.driver.find_element_by_id("cab.snapp.passenger:id/view_main_header_back_ib").click()

        self.driver.find_element_by_id("cab.snapp.passenger:id/view_main_header_search_ib").click()

        self.driver.find_element_by_id("cab.snapp.passenger:id/view_main_header_drawer_ib").click()
        

        '''
        # close the pop-up by clicking
        time.sleep(5)
        gesture = TouchAction(self.driver)
        gesture.tap(x=900, y=1200).perform()
        '''
        search = self.driver.find_element_by_id("cab.snapp.passenger:id/view_search_geocode_et")

        search.send_keys('اکباتان تهران')
        self.driver.press_keycode(84)
        self.driver.find_element_by_xpath("//android.widget.TextView[@bounds='[703,669][912,737]']").click()

        self.driver.find_element_by_id("cab.snapp.passenger:id/view_main_location_selector_ib").click()

        
        self.driver.find_element_by_id("cab.snapp.passenger:id/view_main_header_search_ib").click()
        
        search = self.driver.find_element_by_id("cab.snapp.passenger:id/view_search_geocode_et")
        search.send_keys('میدان آزادی تهران')
        self.driver.press_keycode(84)
       

        self.driver.find_element_by_xpath("//android.widget.TextView[@bounds='[721,522][912,590]']").click()
        self.driver.find_element_by_id("cab.snapp.passenger:id/view_main_location_selector_ib").click()

        time.sleep(3)
        gesture = TouchAction(self.driver)
        time.sleep(5)
        gesture.tap(x=600, y=600).perform()

        '''
        #close options by clicking the button
        self.driver.find_element_by_id("cab.snapp.passenger:id/view_ride_request_footer_options_button").click()
        self.driver.find_element_by_id("cab.snapp.passenger:id/view_main_header_back_ib").click()
        '''

        self.driver.find_element_by_id("cab.snapp.passenger:id/view_ride_request_footer_action_button").click()


if __name__ == '__main__':
    unittest.main()